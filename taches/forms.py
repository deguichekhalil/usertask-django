from django import forms
from django.db.models import fields
from django.db.models.base import Model
from django.forms import ModelForm
from .models import *

class UserForm(ModelForm):
    use_required_attribute = False
    class Meta : 
        model = Utilisateur
        fields = ('nom',)

class TacheForm(forms.ModelForm):
    use_required_attribute = False
    class Meta : 
        model = Tache
        fields = ('titre','utilisateur',)



# creer un formulaire basé sur notre model avec tout les champs pour les mettre dans le formulaire